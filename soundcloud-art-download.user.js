// ==UserScript==
// @name        SoundCloud album art download
// @match       https://soundcloud.com/*
// @grant       GM_download
// @version     1.0
// @author      redbluescreen
// @description Doesn't support Greasemonkey
// ==/UserScript==

let currentPath = null

const interval = setInterval(() => {
  if (currentPath === document.location.pathname) return

  const artworkWrapper = document.querySelector("div.listenArtworkWrapper")
  if (!artworkWrapper) return

  currentPath = document.location.pathname

  const image = artworkWrapper.querySelector("span.image__full")

  const artUrl = image.style["background-image"].match(/"(.*)"/)[1]
  const originalArtUrl = artUrl.replace("t500x500", "original")

  artworkWrapper.style.position = "relative"

  const link = document.createElement("a")
  link.href = "#"
  link.innerText = "Download"
  link.style.position = "absolute"
  link.style.bottom = "0"
  link.style.right = "0"
  link.style["margin-bottom"] = "-18px"
  link.style.display = "inline-block"
  link.style.color = "white"

  link.addEventListener("click", (e) => {
    GM_download(
      originalArtUrl,
      originalArtUrl.substring(originalArtUrl.lastIndexOf("/") + 1)
    )
    e.preventDefault()
  })

  artworkWrapper.appendChild(link)
}, 1000)
