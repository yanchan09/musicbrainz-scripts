// ==UserScript==
// @name     Copy tracklist from OTOTOY
// @grant    GM.setClipboard
// @match    https://ototoy.jp/_/default/p/*
// @version  1.0
// @author   redbluescreen
// ==/UserScript==

const btn = document.createElement("button")
btn.innerText = "Copy tracklist"
btn.style["margin-top"] = "8px"
btn.style["padding"] = "0px 4px"
btn.addEventListener("click", function () {
  GM.setClipboard(generateTracklist())
})

document.querySelector("div.album-tracks").appendChild(btn)

function generateTracklist() {
  const tracks = document.querySelectorAll(
    "#tracklist > tbody > tr:not(:first-child)"
  )
  let tracklist = ""
  for (const track of tracks) {
    const num = track.querySelector("td.num").textContent.trim()
    const cols = track.querySelectorAll("td.item")
    const duration = cols[1].innerText
    let title = cols[0].querySelector("span[id^=title]").innerText
    const artists = Array.from(cols[0].querySelectorAll("a.artist")).map(
      (a) => a.textContent
    )
    if (artists.length) {
      title += ` - ${artists.join(" & ")}`
    }
    tracklist += `${num}. ${title} (${duration})\n`
  }
  return tracklist
}
